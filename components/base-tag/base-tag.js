
// https://lit.dev/docs/getting-started/#use-bundles
// https://open-wc.org/codelabs/basics/web-components.html#0
// https://arq-bbva-cells-files.s3.amazonaws.com/cells/cells-codelabs/_site/codelabs/lit-element-basics/index.html#0
// import { LitElement, html, css } from 'https://unpkg.com/lit-element/lit-element.js?module';
// import {LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
// Otros imports

let styles = css`

`;

class baseTag extends LitElement {
  static get is() {
    return 'base-tag';
  }

  static get styles() {
    return [styles];
  }

  static get properties() {
    return {
      variable: { type: Array }
    };
  }

  constructor() {
    super();
    this.variable = [];
  }

  async connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
      <div class="">
      </div>
    `;
  }
};

customElements.define('base-tag', baseTag);