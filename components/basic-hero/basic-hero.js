
// https://lit.dev/docs/getting-started/#use-bundles
// https://open-wc.org/codelabs/basics/web-components.html#0
// https://arq-bbva-cells-files.s3.amazonaws.com/cells/cells-codelabs/_site/codelabs/lit-element-basics/index.html#0
// import { LitElement, html, css } from 'https://unpkg.com/lit-element/lit-element.js?module';
// import {LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

/**
 * Basic Hero - Carlos Manuel Diaz Jorge
 * Vanilla Hero Web Component with Background Image with Lazy Load
 * Web Component sin frameworks con carga perezosa de imágenes de fondo, cargadas desde el css (background-image style)
 * https://lenguajejs.com/webcomponents/
 * https://web.dev/lazy-loading-images/
 */

import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
// Otros imports

let stylesBasicHero = css`
  :host {
    display: block;
    transition: background-image 0.5s ease;
    margin: 70px 0;
    border-top: 1px solid #b2b1b1;
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif
  }

  .basic-hero {
    display: flex;
  }

  .hero-bg-container {
    background-image: none;
    background-size: cover;
    width: 100%;
  }
  
  .hero-bg {
    background-position: 50%;
    background-size: cover;
    height: 335px;
  }
 
  .visible1 {
    background-image: var(--basic-hero-background-image1, none);
  }
 
  .visible2 {
    background-image: var(--basic-hero-background-image2, none);
  }
 
  .visible3 {
    background-image: var(--basic-hero-background-image3, none);
  }
 
  .visible4 {
    background-image: var(--basic-hero-background-image4, none);
  }

  .hero-text {
    position: absolute;
    top: 10%;
    width: 33%;
    height: 190px;
    color: rgb(51, 51, 51);
    background-color: rgba(255, 255, 255, 0.6);
    padding: 3rem;
  }

  .hero-text.left {
    left: 10%;
  }

  .hero-text.right {
    left: 50%;
  }

  .animated-title {
    color: #333;
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    height: 90vmin;
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
  }

  .animated-title > span {
    height: 50%;
    overflow: hidden;
    position: absolute;
    width: 100%;
  }

  .animated-title>span span {
    font-size: 3vw;
    padding: 2vmin 0;
    position: absolute;
    left: 5px;
  }

  .animated-title>span span span {
    display: block;
  }

  .animated-title>span.text-top {
    border-bottom: 0.1vmin solid #000;
    top: 0;
  }

  .animated-title>span.text-top span {
    animation: showTopText 1s;
    animation-delay: 0.5s;
    animation-fill-mode: forwards;
    bottom: 0;
    transform: translate(0, 100%);
    width: 100%;
  }

  @keyframes showTopText {
    0% {
      transform: translate3d(0, 100%, 0);
    }

    40%,
    60% {
      transform: translate3d(0, 40%, 0);
    }

    100% {
      transform: translate3d(0, 0, 0);
    }
  }

  /* .animated-title>span.text-top span span:first-child {
    color: #767676;
  } */

  .animated-title>span.text-bottom {
    bottom: 0;
  }

  .animated-title>span.text-bottom span {
    font-size: 1.2vw;
    font-weight: 200;
    animation: showBottomText 0.5s;
    animation-delay: 1.75s;
    animation-fill-mode: forwards;
    top: 0;
    left: 10px;
    transform: translate(0, -100%);
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 4;
    text-overflow: ellipsis;
    overflow: hidden;
  }

  @keyframes showBottomText {
    0% {
      transform: translate3d(0, -100%, 0);
    }

    100% {
      transform: translate3d(0, 0, 0);
    }
  }

  @media (max-width: 767px) {
    :host {
      margin: 0 0;
    }

    .basic-hero {
      flex-direction: column;
      width: 100%;
    }

    .hero-bg-container {
      width: 100%;
      margin-top: 70px;
    }
    
    .hero-bg {
      background-position: 70% center;
      background-size: cover;
    }

    .animated-title {
      color: rgb(51, 51, 51);
      font-family: "Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS", sans-serif;
      height: 90vmin;
      left: 50%;
      position: absolute;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 80%;
    }

    .hero-text {
      position: relative;
      width:100%;
      padding: 0;
    }

    .animated-title > span {
      width: 90%;
    }

    .animated-title > span span {
      font-size: 30px;
      padding: 2px;
    }

    .animated-title > span.text-bottom span {
      font-size: 18px;
      text-align: justify;
    }

    .hero-text.left {
      left: 0;
    }
  }

  @media (max-width: 480px) {
    .animated-title {
      width: 90%;
    }
    .animated-title > span span {
      font-size: 25px;
      padding: 2px;
    }
    .animated-title > span {
      width: 100%;
    }
  }
`;

class BasicHero extends LitElement {
  static get is() {
    return 'basic-hero';
  }

  static get styles() {
    return [stylesBasicHero];
  }
  
  static get properties() {
    return {
      side: { type: String },
      header: { type: String },
      text: { type: String },
      visible: { type: Boolean, attribute: false },
      _cta: { type: String, attribute: false }
    };
  }

  constructor() {
    super();
    this.visible = false;
  }

  connectedCallback() {
    super.connectedCallback();
    this.observeVisibility();
  }

  observeVisibility() {
    const observer = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting) {
        this.visible = true;
        observer.disconnect();
      }
    });

    observer.observe(this);
  }

  _splitTitle() {
    let mitades = [];
    const mitad = this.header.length / 2;
    let espacio = 0;
    const espacioAnt = this.header.lastIndexOf(' ', Math.ceil(this.header.length / 2));
    const espacioPost = this.header.indexOf(' ', Math.ceil(this.header.length / 2));
    if (mitad - espacioAnt >= espacioPost - mitad) {
      espacio = espacioPost;
    } else {
      espacio = espacioAnt;
    }
    mitades = [this.header.slice(0, espacio), this.header.slice(espacio + 1)];
    return mitades;
  }

  render() {
    let mitades = this._splitTitle();
    const numImg = Math.floor(Math.random() * 4) + 1;
    return html`
      <div class="basic-hero">
        <div class="hero-bg-container">
          <div class="hero-bg${this.visible ? ' visible' + numImg : ''}"></div>
        </div>
        <div class="hero-text ${this.side}">
          <h1 class="animated-title">
            <span class="text-top">
              <span>
                <span>${mitades[0]}<br>${mitades[1]}</span>
              </span>
            </span>
            <span class="text-bottom">
              <span>${this.text}</span>
            </span>
          </h1>
        </div>
      </div>
    `;
  }
}

customElements.define('basic-hero', BasicHero);
