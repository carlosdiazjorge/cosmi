import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
import { pubsub } from '../pubsub/pubsub.js';

let styles = css`
  .gallery-item {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 20px;
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 5px;
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0.3);
    cursor: pointer;
  }

  .gallery-item:hover {
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
  }

  .gallery-item img {
    width: 100%;
  }

  .gallery-item h3 {
    margin: 10px 0;
    font-size: 1.2em;
  }

  .gallery-item p {
    margin: 5px 0;
    font-size: 1em;
  }

  .gallery-item .price {
    margin-top: 10px;
    font-weight: bold;
    font-size: 1.2em;
  }

  button {
    color: #ffffff;
    background-color: #2d63c8;
    font-size: 14px;
    border: 1px solid #2d63c8;
    border-radius: 10px;
    padding: 4px 10px;
    letter-spacing: 1px;
    cursor: pointer;
    margin-top: 10px;
  }

  button:hover {
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.6);
  }

  button:focus {
    color: #ffffff;
    background-color: #46b432;
    border: 1px solid #46b432;
  }
`;

class GalleryItem extends LitElement {
  static get is() {
    return 'gallery-item';
  }

  static get styles() {
    return [styles];
  }

  static get properties() {
    return {
      imgS: { type: String },
      imgM: { type: String },
      imgL: { type: String },
      title: { type: String },
      description: { type: String },
      price: { type: Number },
      code: { type: String }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <div class="gallery-item">
        <!-- srcset: lista de imágenes con diferentes tamaños o resoluciones -->
        <!-- srcset="ruta-de-la-imagen tamaño-de-la-imagen, ruta-de-la-imagen tamaño-de-la-imagen, ... -->

        <!-- sizes: ancho de la imagen que se mostrará en diferentes tamaños de pantalla -->
        <!-- sizes="(condición) ancho-de-la-imagen, (condición) ancho-de-la-imagen, ..." -->
        <img
          srcset="${this.imgM} 450w, ${this.imgL} 640w"
          sizes="(max-width: 480px) 640px, (max-width: 767px) 450px, 450px"
          src="${this.imgL}"
          type="image/webp"
          title="${this.title}"
          alt="${this.title}"
          loading="lazy"
        />
        <h2>${this.title}</h2>
        <p>${this.description}</p>
        <div class="price">${this.price.toFixed(2)}€</div>
        <button @click="${this.publishMessage}" type="button" name="add-basket">Añadir al carrito</button>
      </div>
      `;
  }

  publishMessage() {
    const message = { msg: this.code };
    pubsub.publish('customMessage', message);
  }
};

customElements.define('gallery-item', GalleryItem);