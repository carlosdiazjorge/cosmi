
// https://lit.dev/docs/getting-started/#use-bundles
// https://open-wc.org/codelabs/basics/web-components.html#0
// https://arq-bbva-cells-files.s3.amazonaws.com/cells/cells-codelabs/_site/codelabs/lit-element-basics/index.html#0
// import { LitElement, html, css } from 'https://unpkg.com/lit-element/lit-element.js?module';
// import {LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
import '../gallery-item/gallery-item.js';
import { items } from '../../db/items.js';

let stylesItemsGallery = css`
  .gallery {
    max-width: 1200px;
    padding: 0;
    margin: 20px auto;
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(40ch, 1fr));
    gap: 1rem;
  }
`;

class ItemsGallery extends LitElement {
  static get is() {
    return 'items-gallery';
  }

  static get styles() {
    return [stylesItemsGallery];
  }

  static get properties() {
    return {
      products: { type: Array }
    };
  }

  constructor() {
    super();
    this.products = [];
  }
  
  async connectedCallback() {
    super.connectedCallback();
    this.products = items.list;
  }

  render() {
    return html`
      <div class="gallery">
        ${this.products.map(
          product => html`
            <gallery-item
              imgs="${product.imgS}"
              imgm="${product.imgM}"
              imgl="${product.imgL}"
              title="${product.title}"
              description="${product.description}"
              price="${product.price}"
              code="${product.code}"
            ></gallery-item>
          `
        )}
      </div>
    `;
  }
};

customElements.define('items-gallery', ItemsGallery);
