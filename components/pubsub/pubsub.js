/**
 * Publish / Subscribe Pattern
 * To comunicate between sibling components
 */
class PubSub {
  constructor() {
    this.events = {};
  }

  subscribe(eventName, fn) {
    if (!this.events[eventName]) {
      this.events[eventName] = [];
    }
    this.events[eventName].push(fn);
  }

  unsubscribe(eventName, fn) {
    if (!this.events[eventName]) return;
    const index = this.events[eventName].indexOf(fn);
    if (index > -1) {
      this.events[eventName].splice(index, 1);
    }
  }

  publish(eventName, data) {
    if (!this.events[eventName]) return;
    this.events[eventName].forEach(fn => {
      fn(data);
    });
  }
}

export const pubsub = new PubSub();
