import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
import { pubsub } from '../pubsub/pubsub.js';
import { items } from '../../db/items.js';

let stylesShoppingCart = css`
  /* Styles the sidenav */
  .sidenav {
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 2;
    top: 0;
    right: -10px;
    background-color: #EEE;
    background-color: whitesmoke;
    overflow: hidden;
    transition: 0.5s;
    padding-top: 60px;
    padding-left: 10px;
    min-height: 100vh;
  }

  .sidenav.open {
    width: 260px;
    box-shadow: rgb(64 64 64 / 35%) -8px 5px 15px;
  }

  .sidenav a {
    text-decoration: none;
    font-size: 18px;
    color: #818181;
    display: block;
    transition: 0.3s;
    cursor: pointer;
  }

  .sidenav .closebtn {
    position: absolute;
    top: 10px;
    right: 15px;
    font-size: 36px;
    margin-left: 50px;
  }


  .icon {
    position: fixed;
    top: 25px;
    right: 25px;
    font-size: 36px;
    cursor: pointer;
  }

  .carrito {
    display: block;
    max-width: 24px;
  }

  .products-cart {
    background-color: whitesmoke;
    width: calc(100% - 10px);
    scrollbar-width: thin;
    overflow: hidden auto;
    max-height: 83vh;
  }

  .products-cart::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  .products-cart::-webkit-scrollbar-track {
    border-radius: 10px;
    background: rgba(0,0,0,0.1);
    border: 1px solid #ccc;
  }

  .products-cart::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background: linear-gradient(left, #fff, #e4e4e4);
    border: 1px solid #aaa;
  }

  .products-cart::-webkit-scrollbar-thumb:hover {
    background: #fff;
  }

  .products-cart::-webkit-scrollbar-thumb:active {
    background: linear-gradient(left, #22ADD4, #1E98BA);
  }

  .action {
    width: 250px;
    margin: 20px auto;
    position: absolute;
    bottom: 50px;
  }

  button {
    color: #ffffff;
    background-color: #2d63c8;
    font-size: 19px;
    border: 1px solid #2d63c8;
    border-radius: 10px;
    padding: 5px 20px;
    letter-spacing: 1px;
    cursor: pointer;
    width: 100%;
    margin-top: 10px;
  }

  button:hover {
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.6);
  }

  button:focus {
    color: #ffffff;
    background-color: #46b432;
    border: 1px solid #46b432;
  }

  .header-products {
    font-size: 14px;
    font-weight: bold;
    width: 250px;
  }

  .cart-product {
    width: 230px;
    padding: 5px 0px 20px 0px;
    border-bottom: 1px solid gray;
  }

  .cart-product {
    display: grid;
    grid-template-columns: 30% 70%;
    grid-template-rows: 70% 30%;
    gap: 0px 0px;
    grid-auto-flow: row;
    grid-template-areas:
      "prodImg prodName"
      "prodNumbers prodNumbers";
    height: 110px;
  }

  .prodName {
    grid-area: prodName;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    text-overflow: ellipsis;
    overflow: hidden;
    padding-left: 5px;
  }

  .prodNumbers {
    grid-area: prodNumbers;
  }

  .prodImg {
    grid-area: prodImg;
  }

  .prodDel {
    float: right;
    padding: 4px 4px 0px 4px;
    border: 1px solid black;
    cursor: pointer;
  }

  .product-img {
    width: 100%;
    grid-area: product-img;
  }

  .product-title {
    font-weight: bold;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    text-overflow: ellipsis;
    overflow: hidden;
  }

  .prodNumbers {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(20px, 1fr));
    gap: 0px;
    place-items: center;
  }

`;

class ShoppingCart extends LitElement {
  static get is() {
    return 'shopping-cart';
  }

  static get styles() {
    return [stylesShoppingCart];
  }

  static get properties() {
    return {
      open: { type: Boolean },
      products: { type: Array },
      productsList: { type: Array },
      options: { type: Array }
    };
  }

  constructor() {
    super();
    this.open = false;
    this.products = [];
    this.productsList = [];
    this.options = Array.from({length: 100}, (_, i) => i + 1);
    this.handleCustomMessage = this.handleCustomMessage.bind(this);
  }

  connectedCallback() {
    super.connectedCallback();
    pubsub.subscribe('customMessage', this.handleCustomMessage);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    pubsub.unsubscribe('customMessage', this.handleCustomMessage);
  }

  handleCustomMessage(price) {
    let code = price.msg;
    this.open = true;
    let newItem = {
      "price": code,
      "quantity": 1,
      "adjustable_quantity": {
        "enabled": true,
        "minimum": 0,
        "maximum": this.options.length
      }
    };
    if (!this.products.some(objeto => objeto.price === newItem.price)) {
      this.products.push(newItem);
      const posDB = items.list.findIndex(objeto => objeto.code === code);
      this.productsList.push({...items.list[posDB]});
    } else {
      const pos = this.products.findIndex(objeto => objeto.price === code);
      if (this.products[pos].quantity < this.options.length) {
        this.products[pos].quantity++;
        this.productsList[pos].quantity++;
        let subtotal = this.productsList[pos].price * this.productsList[pos].quantity;
        this.productsList[pos].subtotal = subtotal.toFixed(2);
      }
    }
    this.requestUpdate();
  }

  _openPanel(ev) {
    this.open = !this.open;
  }

  /**
   * Invocación de una función Lambda desde un navegador
   * Debe estar cargada previamente la librería de AWS-SDK, pero únicamente con los servicios usados: cognito y lambdas
   * Para personalizar la librería usamos https://sdk.amazonaws.com/builder/js/ y alojamos el fichero resultante en nuestro hosting.
   * 
   * https://docs.aws.amazon.com/es_es/sdk-for-javascript/v3/developer-guide/cross_LambdaForBrowser_javascript_topic.html
   */
  _tramitarPedido() {
    // Configure AWS SDK for JavaScript & set region and credentials
    // Initialize the Amazon Cognito credentials provider
    AWS.config.region = 'eu-west-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: 'eu-west-1:e373b2f5-8299-4b9a-bc61-7cdbb5ac8830',
    });
    /// Prepare to call Lambda function
    let lambda = new AWS.Lambda({ region: 'eu-west-1', apiVersion: '2015-03-31' });
    const params = {
      FunctionName: 'checkout-stripe', /* required */
      InvocationType: 'RequestResponse',
      Payload: JSON.stringify({"body": {"web": "tienda-basica", "products": this.products}}), /* (Buffer, Typed Array, Blob, String) */
      LogType: 'None'
    };
    console.log(params);

    // Call the Lambda function
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html#invoke-property
    lambda.invoke(params, function (err, data) {
      if (err) {
        prompt(err);
      } else {
        let response = JSON.parse(data.Payload);
        console.log(response);
        if (response.headers.Location) {
          if (response.headers && response.headers.Location !== '') {
            window.location.href = response.headers.Location;
          }
        } else {
          console.log(response.errorMessage);
        }
      }
    });
  }

  _onOptionSelected(ev) {
    let idProducto = parseInt(ev.target.dataset.idproduct);
    this.products[idProducto].quantity = parseInt(ev.target.value);
    this.productsList[idProducto].quantity = parseInt(ev.target.value);
    let subtotal = this.productsList[idProducto].price * this.productsList[idProducto].quantity;
    this.productsList[idProducto].subtotal = subtotal.toFixed(2);
    this.requestUpdate();
  }

  _delProduct(ev) {
    let idProducto = parseInt(ev.currentTarget.dataset.idprod);
    this.products.splice(idProducto, 1);
    this.productsList.splice(idProducto, 1);
    this.requestUpdate();
  }

  render() {
    return html`
      <div class="icon" @click="${this._openPanel}">${this._shoppingCartIcon()}</div>
      <div id="right-sidenav" class="sidenav${this.open ? ' open' : ''}">
        <div class="cabecera-carrito">
          <a href="#" class="closebtn" rel="nofollow" @click="${this._openPanel}">\&times;</a>
        </div>
        <div class="header-products">Lista de productos</div>
        <div class="products-cart">
          ${this.productsList.map((product, idproduct) => {
            return html`
              <div class="cart-product">
                <div class="prodImg">
                  <img class="product-img" src="${product.imgS}" alt="${product.title}" title="${product.title}" loading="lazy" />
                </div>
                <div class="prodName">
                  <div class="prodDel" data-idprod="${idproduct}" @click="${this._delProduct}">${this._trashCartIcon()}</div>
                  <div class="product-title">${product.title}</div>
                  ${product.description}
                </div>
                <div class="prodNumbers">
                  <div class="quantity">
                    <select @change="${this._onOptionSelected}" data-idproduct="${idproduct}">
                      ${this.options.map(option => html`
                        <option value="${option}" ?selected="${option === product.quantity}">
                          ${option}
                        </option>
                      `)}
                    </select>
                  </div>
                  <div> x </div>
                  <div class="price">${product.price}€</div>
                  <div> = </div>
                  <div class="subtotal">${product.subtotal}€</div>
                </div>
              </div>
            `;
          })}
        </div>
        <div class="action">
          <button type="button" @click="${this._tramitarPedido}" name="checkout" ?hidden="${!this.products.length}">Tramitar pedido</button>
        </div>
      </div>
    `;
  }

  _shoppingCartIcon() {
    return html`
      <svg class="carrito" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
        width="24" height="24" viewBox="0 0 256 256" xml:space="preserve">
        <title>Carrito</title>
        <defs></defs>
        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;"
          transform="translate(1.4065934065934016 1.4065934065934016) scale(2.81 2.81)">
          <path
            d="M 73.713 65.44 H 27.689 c -3.566 0 -6.377 -2.578 -6.686 -6.13 c -0.21 -2.426 0.807 -4.605 2.592 -5.939 L 16.381 21.07 c -0.199 -0.889 0.017 -1.819 0.586 -2.53 s 1.431 -1.124 2.341 -1.124 H 87 c 0.972 0 1.884 0.471 2.446 1.263 c 0.563 0.792 0.706 1.808 0.386 2.725 l -7.798 22.344 c -1.091 3.13 -3.798 5.429 -7.063 5.999 l -47.389 8.281 c -0.011 0.001 -0.021 0.003 -0.032 0.005 c -0.228 0.04 -0.623 0.126 -0.568 0.759 c 0.056 0.648 0.48 0.648 0.708 0.648 h 46.024 c 1.657 0 3 1.343 3 3 S 75.37 65.44 73.713 65.44 z"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(105,105,105); fill-rule: nonzero; opacity: 1;"
            transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
          <circle cx="28.25" cy="75.8" r="6.5"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(105,105,105); fill-rule: nonzero; opacity: 1;"
            transform="  matrix(1 0 0 1 0 0) " />
          <circle cx="68.28999999999999" cy="75.8" r="6.5"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(105,105,105); fill-rule: nonzero; opacity: 1;"
            transform="  matrix(1 0 0 1 0 0) " />
          <path
            d="M 19.306 23.417 c -1.374 0 -2.613 -0.95 -2.925 -2.347 l -1.375 -6.155 c -0.554 -2.48 -2.716 -4.212 -5.258 -4.212 H 3 c -1.657 0 -3 -1.343 -3 -3 s 1.343 -3 3 -3 h 6.749 c 5.372 0 9.942 3.662 11.113 8.904 l 1.375 6.155 c 0.361 1.617 -0.657 3.221 -2.274 3.582 C 19.742 23.393 19.522 23.417 19.306 23.417 z"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(105,105,105); fill-rule: nonzero; opacity: 1;"
            transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
        </g>
      </svg>
    `;
  }

  _trashCartIcon() {
    return html`
      <svg class="papelera" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 105.7 122.88" width="16" height="16">
        <title>eliminar</title>
        <path d="M30.46,14.57V5.22A5.18,5.18,0,0,1,32,1.55v0A5.19,5.19,0,0,1,35.68,0H70a5.22,5.22,0,0,1,3.67,1.53l0,0a5.22,5.22,0,0,1,1.53,3.67v9.35h27.08a3.36,3.36,0,0,1,3.38,3.37V29.58A3.38,3.38,0,0,1,102.32,33H98.51l-8.3,87.22a3,3,0,0,1-2.95,2.69H18.43a3,3,0,0,1-3-2.95L7.19,33H3.37A3.38,3.38,0,0,1,0,29.58V17.94a3.36,3.36,0,0,1,3.37-3.37Zm36.27,0V8.51H39v6.06ZM49.48,49.25a3.4,3.4,0,0,1,6.8,0v51.81a3.4,3.4,0,1,1-6.8,0V49.25ZM69.59,49a3.4,3.4,0,1,1,6.78.42L73,101.27a3.4,3.4,0,0,1-6.78-.43L69.59,49Zm-40.26.42A3.39,3.39,0,1,1,36.1,49l3.41,51.8a3.39,3.39,0,1,1-6.77.43L29.33,49.46ZM92.51,33.38H13.19l7.94,83.55H84.56l8-83.55Z"/>
      </svg>
    `;
  }
};

customElements.define('shopping-cart', ShoppingCart);