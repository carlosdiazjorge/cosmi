// https://lit.dev/docs/getting-started/#use-bundles
// https://open-wc.org/codelabs/basics/web-components.html#0
// https://arq-bbva-cells-files.s3.amazonaws.com/cells/cells-codelabs/_site/codelabs/lit-element-basics/index.html#0
// import { LitElement, html, css } from 'https://unpkg.com/lit-element/lit-element.js?module';
// import {LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

/**
 * © Carlos Manuel Diaz Jorge
 * Web Components
 * https://lenguajejs.com/webcomponents/
 * https://web.dev/lazy-loading-images/
 */
'use strict';
import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
import { seo, textos, products } from '../db/content.js';

window.addEventListener('load', function () {
  window.document.querySelector('title').textContent = seo.title;
  window.document.querySelector('meta[name="title"]').setAttribute('content', seo.title);
  window.document.querySelector('meta[name="description"]').setAttribute('content', seo.description);
  window.document.querySelector('meta[name="robots"]').setAttribute('content', seo.robots);
  window.document.querySelector('meta[name="keywords"]').setAttribute('content', seo.keywords);
  window.document.querySelector('meta[name="author"]').setAttribute('content', seo.author);
  window.document.querySelector('meta[property="og:title"]').setAttribute('content', seo.ogtitle);
  window.document.querySelector('meta[property="og:image"]').setAttribute('content', seo.ogimage);
  window.document.querySelector('meta[property="og:description"]').setAttribute('content', seo.ogdescription);
  window.document.querySelector('meta[property="og:site_name"]').setAttribute('content', seo.ogsitename);
  window.document.querySelector('link[rel="canonical"]').setAttribute('content', seo.canonical);
  setTimeout(function () {
    var iframe = document.createElement('iframe');
    iframe.src = 'https://www.googletagmanager.com/ns.html?id=GTM-5DMG7ZJ';
    iframe.height = 0;
    iframe.width = 0;
    iframe.style.display = 'none';
    iframe.style.visibility = 'hidden';
    document.body.appendChild(iframe);
  }, 1500);
});
//##################################################################################################################
class headerNavBar extends LitElement {
  static get is() {
    return 'header-navbar';
  }

  createRenderRoot() {
    return this;
  }

  static get styles() {
    return [];
  }

  static get properties() {
    return {
      variable: { type: Array }
    };
  }

  constructor() {
    super();
    this.variable = [];
  }

  async connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
      <nav class="navbar">
        <a href="#" class="logo">
          <svg viewBox="0 0 632.327 211.735" xmlns="http://www.w3.org/2000/svg">
            <g transform="matrix(1, 0, 0, 1, 0, 7)">
              <path style="fill: rgb(191, 219, 152); stroke: rgb(191, 219, 152);" d="M 54.14 93.411 C 79.272 93.356 92.194 90.4 92.194 90.4 L 106.359 87.583 C 106.359 87.583 106.848 86.449 122.078 91.633 C 128.277 93.743 145.146 87.139 151.91 94.529 C 151.049 115.411 152.913 117.476 150.603 134.117 C 141.872 153.613 133.734 175.059 104.138 178.29 C 71.359 179.272 55.807 139.759 55.807 139.759 C 55.807 139.759 53.35 118.912 54.14 93.411 Z"/>
              <path d="M 90.923 117.762 C 92.095 116.591 92.095 114.691 90.924 113.52 C 82.98 105.571 70.045 105.57 62.088 113.518 C 60.916 114.689 60.915 116.589 62.086 117.761 C 62.672 118.347 63.44 118.64 64.208 118.64 C 64.975 118.64 65.743 118.348 66.328 117.763 C 71.945 112.152 81.074 112.152 86.68 117.761 C 87.851 118.933 89.751 118.933 90.923 117.762 Z" style="stroke: rgb(51, 51, 51); fill: rgb(51, 51, 51);"/>
              <path d="M 141.269 117.762 C 142.441 116.591 142.441 114.691 141.27 113.52 C 137.421 109.668 132.3 107.546 126.852 107.546 C 121.403 107.546 116.283 109.668 112.433 113.52 C 111.262 114.692 111.263 116.591 112.434 117.762 C 113.02 118.348 113.787 118.64 114.555 118.64 C 115.323 118.64 116.091 118.347 116.677 117.761 C 119.393 115.043 123.007 113.546 126.852 113.546 C 130.697 113.546 134.31 115.043 137.027 117.761 C 138.198 118.933 140.097 118.933 141.269 117.762 Z" style="stroke: rgb(51, 51, 51); fill: rgb(51, 51, 51);"/>
              <path d="M 101.684 166.989 C 115.996 166.989 127.64 155.356 127.64 141.058 C 127.64 137.156 124.463 133.981 120.558 133.981 L 82.798 133.981 C 78.9 133.981 75.728 137.156 75.728 141.058 C 75.728 155.356 87.372 166.989 101.684 166.989 Z M 82.798 139.981 L 120.558 139.981 C 121.145 139.981 121.64 140.474 121.64 141.058 C 121.64 152.048 112.688 160.989 101.684 160.989 C 90.68 160.989 81.728 152.048 81.728 141.058 C 81.728 140.474 82.218 139.981 82.798 139.981 Z" style="stroke: rgb(51, 51, 51); fill: rgb(51, 51, 51);"/>
              <path d="M 199.425 41.896 C 199.252 40.005 197.791 39.25 196.378 38.52 C 195.933 38.29 195.38 38.004 194.733 37.642 L 181.473 30.228 L 172.593 14.457 C 171.919 13.208 170.733 12.491 169.421 12.509 C 168.097 12.486 166.921 13.213 166.249 14.457 L 158.574 28.087 L 158.275 27.788 C 142.603 12.173 123.563 4.255 101.684 4.255 C 87.642 4.255 74.603 7.591 62.895 14.19 C 62.888 14.194 62.87 14.205 62.859 14.211 L 60.019 12.623 L 51.459 -2.573 C 50.791 -3.783 49.632 -4.493 48.365 -4.493 C 48.331 -4.493 48.296 -4.493 48.262 -4.493 C 46.994 -4.493 45.836 -3.783 45.168 -2.573 L 36.619 12.624 L 23.105 20.18 C 20.909 21.403 19.174 22.369 19.174 24.575 C 19.174 26.782 20.909 27.748 23.105 28.97 L 36.618 36.526 L 36.919 37.061 C 31.642 43.847 26.976 51.579 23.034 60.077 C 22.337 61.58 22.99 63.364 24.492 64.061 C 24.901 64.251 25.33 64.34 25.753 64.34 C 26.885 64.34 27.968 63.697 28.476 62.602 C 31.84 55.35 35.75 48.698 40.125 42.765 L 45.167 51.733 C 45.835 52.944 46.994 53.655 48.262 53.655 C 48.279 53.655 48.296 53.655 48.313 53.655 C 49.598 53.689 50.782 52.961 51.458 51.734 L 60.018 36.526 L 73.533 28.97 C 75.729 27.747 77.464 26.782 77.464 24.575 C 77.464 22.368 75.729 21.402 73.533 20.179 L 69.097 17.699 C 79.025 12.763 89.967 10.254 101.683 10.254 C 121.918 10.254 139.533 17.583 154.036 32.034 L 154.071 32.07 L 144.103 37.643 C 143.461 38.004 142.907 38.29 142.463 38.519 C 141.05 39.249 139.589 40.005 139.416 41.895 C 139.402 42.049 139.395 42.269 139.424 42.531 C 139.395 42.793 139.402 43.013 139.416 43.167 C 139.589 45.057 141.049 45.812 142.462 46.542 C 142.907 46.772 143.461 47.058 144.106 47.42 L 157.367 54.845 L 166.257 70.632 C 166.928 71.849 168.097 72.565 169.374 72.564 C 169.409 72.565 169.443 72.565 169.477 72.565 C 170.748 72.565 171.911 71.85 172.583 70.632 L 175.871 64.792 C 183.563 82.164 187.926 102.062 187.926 120.137 L 187.926 164.023 C 187.926 170.385 186.088 175.219 182.611 178.001 C 179.576 180.43 175.271 181.319 171.095 180.38 L 137.158 172.753 C 149.181 162.904 158.501 149.369 162.126 136.022 C 165.921 135.423 169.838 133.38 172.98 130.281 C 176.595 126.715 178.668 122.264 178.668 118.07 L 178.668 112.163 C 178.668 107.216 174.639 103.19 169.687 103.19 L 163.773 103.19 L 163.773 95.695 C 163.773 94.823 163.786 94.002 163.798 93.233 C 163.864 89.081 163.916 85.802 161.535 83.796 C 159.522 82.102 156.866 82.363 155.042 82.707 C 149.95 83.672 140.214 83.657 129.942 81.45 C 128.321 81.1 126.727 82.133 126.379 83.753 C 126.031 85.373 127.062 86.968 128.682 87.316 C 138.435 89.411 149.22 89.915 156.157 88.603 C 156.938 88.455 157.391 88.444 157.627 88.46 C 157.86 89.286 157.827 91.403 157.8 93.137 C 157.787 93.936 157.774 94.79 157.774 95.695 L 157.774 124.35 C 157.774 126.922 157.462 129.57 156.874 132.251 C 156.814 132.43 156.77 132.618 156.745 132.811 C 152.416 151.363 134.963 171.359 115.431 178.633 C 110.251 180.558 105.883 181.455 101.684 181.455 C 97.477 181.455 93.105 180.558 87.927 178.634 C 86.592 178.136 85.258 177.575 83.934 176.959 C 85.754 175.933 87.118 174.99 87.12 173.02 C 87.122 170.842 85.458 169.915 83.351 168.741 L 70.809 161.726 L 62.885 147.64 C 62.225 146.429 61.074 145.72 59.814 145.72 C 59.78 145.72 59.746 145.72 59.711 145.72 C 58.459 145.72 57.315 146.42 56.652 147.617 L 54.72 151.047 C 48.885 142.053 45.594 132.509 45.594 124.35 L 45.594 106.192 C 45.594 106.192 45.594 106.191 45.594 106.19 L 45.594 106.188 L 45.594 95.656 C 45.59 95.37 45.575 95.027 45.558 94.639 C 45.495 93.258 45.349 90.021 46.349 89.057 C 46.621 88.795 47.126 88.685 47.852 88.728 C 59.884 90.736 88.16 87.87 99.955 75.175 C 100.355 74.759 100.97 74.268 101.651 74.262 C 101.656 74.262 101.66 74.262 101.665 74.262 C 102.342 74.262 102.968 74.736 103.387 75.15 C 106.138 78.122 109.962 80.722 114.755 82.882 C 116.267 83.562 118.043 82.89 118.723 81.38 C 119.404 79.869 118.731 78.093 117.221 77.412 C 113.144 75.575 109.96 73.431 107.757 71.039 C 107.732 71.011 107.706 70.985 107.68 70.959 C 105.93 69.193 103.852 68.262 101.668 68.262 C 101.643 68.262 101.618 68.263 101.594 68.263 C 99.392 68.284 97.321 69.246 95.603 71.046 C 95.593 71.056 95.583 71.067 95.573 71.077 C 85.764 81.657 59.664 84.675 48.72 82.79 C 48.632 82.774 48.543 82.763 48.453 82.756 C 45.236 82.494 43.269 83.692 42.183 84.74 C 39.234 87.586 39.449 92.35 39.565 94.91 C 39.579 95.224 39.592 95.503 39.595 95.695 L 39.595 103.19 L 33.669 103.19 C 28.717 103.19 24.688 107.216 24.688 112.164 L 24.688 118.07 C 24.688 122.262 26.762 126.712 30.377 130.278 C 33.526 133.384 37.449 135.43 41.249 136.026 C 43.133 142.932 46.589 150.052 51.431 156.887 L 48.705 161.726 L 36.174 168.742 C 34.067 169.916 32.403 170.843 32.405 173.021 C 32.408 175.199 34.072 176.121 36.18 177.288 L 39.033 178.885 L 32.267 180.379 C 28.078 181.31 23.768 180.414 20.737 177.982 C 17.272 175.203 15.44 170.375 15.44 164.023 L 15.44 120.138 C 15.44 106.004 18.209 90.197 23.235 75.629 C 23.776 74.063 22.944 72.355 21.378 71.815 C 19.811 71.275 18.103 72.106 17.563 73.672 C 12.325 88.854 9.44 105.356 9.44 120.138 L 9.44 164.024 C 9.44 174.429 13.542 179.903 16.982 182.664 C 20.293 185.32 24.585 186.738 29.021 186.738 C 30.53 186.738 32.056 186.574 33.564 186.238 L 46.904 183.292 L 48.704 184.3 L 56.652 198.419 C 57.31 199.61 58.453 200.305 59.707 200.305 L 59.818 200.305 C 61.079 200.305 62.226 199.604 62.866 198.431 L 70.809 184.3 L 77.62 180.489 C 80.312 181.962 83.067 183.228 85.834 184.258 C 91.704 186.44 96.74 187.456 101.683 187.456 C 106.618 187.456 111.651 186.44 117.523 184.258 C 122.101 182.552 126.548 180.242 130.762 177.466 L 169.779 186.236 C 171.3 186.578 172.837 186.745 174.358 186.745 C 178.776 186.745 183.053 185.333 186.36 182.687 C 189.811 179.925 193.926 174.445 193.926 164.024 L 193.926 120.138 C 193.926 99.844 188.673 77.37 179.5 58.348 L 181.472 54.846 L 194.736 47.42 C 195.379 47.059 195.933 46.773 196.377 46.543 C 197.79 45.813 199.251 45.059 199.424 43.168 C 199.438 43.015 199.445 42.794 199.415 42.532 C 199.446 42.269 199.439 42.049 199.425 41.896 Z M 56.355 31.702 C 55.873 31.971 55.475 32.368 55.205 32.848 L 48.315 45.088 L 41.435 32.85 C 41.164 32.368 40.766 31.971 40.284 31.702 L 27.539 24.575 L 40.283 17.449 C 40.765 17.179 41.163 16.782 41.434 16.301 L 48.315 4.071 L 55.205 16.303 C 55.476 16.783 55.873 17.179 56.355 17.449 L 61.539 20.347 C 61.54 20.348 61.541 20.349 61.543 20.349 L 69.1 24.575 L 56.355 31.702 Z M 169.688 109.191 C 171.332 109.191 172.669 110.525 172.669 112.165 L 172.669 118.071 C 172.669 120.671 171.247 123.565 168.767 126.011 C 167.175 127.582 165.303 128.784 163.438 129.494 C 163.658 127.763 163.774 126.046 163.774 124.352 L 163.774 109.192 L 169.688 109.192 L 169.688 109.191 Z M 34.59 126.007 C 32.11 123.561 30.688 120.668 30.688 118.071 L 30.688 112.164 C 30.688 110.552 32.053 109.19 33.669 109.19 L 39.594 109.19 L 39.594 124.351 C 39.594 126.032 39.708 127.748 39.93 129.488 C 38.063 128.78 36.185 127.58 34.59 126.007 Z M 67.144 179.475 C 66.662 179.745 66.264 180.142 65.994 180.623 L 59.762 191.71 L 53.52 180.622 C 53.25 180.141 52.852 179.745 52.372 179.476 L 40.83 173.013 L 52.372 166.55 C 52.852 166.281 53.249 165.885 53.52 165.405 L 59.762 154.324 L 65.994 165.403 C 66.265 165.885 66.663 166.281 67.144 166.551 L 78.695 173.013 L 67.144 179.475 Z M 177.807 50.022 C 177.326 50.291 176.929 50.688 176.659 51.168 L 173.6 56.6 C 173.535 56.699 173.479 56.802 173.427 56.907 L 169.421 64.021 L 162.183 51.168 C 161.912 50.688 161.515 50.291 161.034 50.022 L 147.656 42.531 L 161.033 35.053 C 161.514 34.784 161.912 34.387 162.183 33.906 L 169.421 21.053 L 176.659 33.906 C 176.93 34.387 177.327 34.784 177.809 35.053 L 191.185 42.531 L 177.807 50.022 Z" style="stroke-width: 0px; fill: rgb(51, 51, 51); paint-order: fill;"/>
            </g>
            <g transform="matrix(1, 0, 0, 1, -146.31206, -75.119973)">
              <g style="" transform="matrix(1.282571, 0, 0, 1.403885, -99.127129, -65.723862)">
                <path d="M 20.433 0.73 Q 17.879 0.73 15.273 -0.026 Q 12.667 -0.782 10.269 -2.32 Q 7.871 -3.857 6.021 -6.125 Q 4.17 -8.392 3.075 -11.389 Q 1.981 -14.387 1.981 -18.035 Q 1.981 -21.632 3.075 -24.525 Q 4.17 -27.418 6.021 -29.607 Q 7.871 -31.797 10.243 -33.256 Q 12.614 -34.716 15.247 -35.471 Q 17.879 -36.227 20.485 -36.279 Q 23.144 -36.279 25.072 -35.706 Q 27.001 -35.133 28.2 -34.481 Q 29.399 -33.83 29.816 -33.569 Q 30.65 -33.048 31.406 -32.396 Q 32.161 -31.744 32.214 -30.806 Q 32.266 -30.181 32.057 -29.686 Q 31.849 -29.19 31.484 -28.669 Q 30.858 -27.783 30.233 -27.366 Q 29.607 -26.949 28.982 -26.949 Q 28.408 -26.949 27.913 -27.262 Q 27.418 -27.574 26.688 -28.044 Q 26.376 -28.252 25.568 -28.695 Q 24.76 -29.138 23.43 -29.503 Q 22.101 -29.868 20.225 -29.868 Q 17.983 -29.868 15.846 -29.086 Q 13.709 -28.304 12.015 -26.766 Q 10.321 -25.229 9.33 -22.961 Q 8.34 -20.694 8.34 -17.775 Q 8.34 -14.856 9.33 -12.588 Q 10.321 -10.321 12.015 -8.783 Q 13.709 -7.245 15.846 -6.464 Q 17.983 -5.682 20.225 -5.682 Q 21.997 -5.682 23.274 -6.021 Q 24.551 -6.359 25.463 -6.855 Q 26.376 -7.35 26.845 -7.61 Q 27.418 -7.975 27.939 -8.288 Q 28.461 -8.601 29.034 -8.601 Q 29.659 -8.601 30.259 -8.184 Q 30.858 -7.767 31.432 -6.828 Q 31.797 -6.255 32.005 -5.734 Q 32.214 -5.213 32.161 -4.639 Q 32.109 -3.701 31.354 -3.049 Q 30.598 -2.398 29.764 -1.929 Q 29.347 -1.668 28.174 -1.016 Q 27.001 -0.365 25.072 0.182 Q 23.144 0.73 20.433 0.73 Z" transform="matrix(1.410424, 0, 0, 1.410429, 353.003082, 204.288284)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 46.935 0.834 Q 44.433 0.834 42.061 -0.13 Q 39.69 -1.095 37.787 -2.867 Q 35.885 -4.639 34.764 -7.037 Q 33.643 -9.435 33.643 -12.302 Q 33.643 -15.064 34.686 -17.462 Q 35.728 -19.86 37.553 -21.684 Q 39.377 -23.509 41.749 -24.525 Q 44.12 -25.542 46.831 -25.542 Q 50.48 -25.542 53.451 -23.795 Q 56.422 -22.049 58.194 -19.078 Q 59.967 -16.107 59.967 -12.354 Q 59.967 -9.383 58.872 -6.959 Q 57.777 -4.535 55.901 -2.789 Q 54.024 -1.043 51.705 -0.104 Q 49.385 0.834 46.935 0.834 Z M 46.831 -5.317 Q 48.551 -5.317 50.141 -6.177 Q 51.731 -7.037 52.747 -8.653 Q 53.764 -10.269 53.764 -12.458 Q 53.764 -14.491 52.851 -16.055 Q 51.939 -17.618 50.376 -18.505 Q 48.812 -19.391 46.779 -19.391 Q 44.85 -19.391 43.286 -18.479 Q 41.723 -17.566 40.81 -15.976 Q 39.898 -14.387 39.898 -12.406 Q 39.898 -10.217 40.889 -8.627 Q 41.879 -7.037 43.469 -6.177 Q 45.059 -5.317 46.831 -5.317 Z" transform="matrix(1.410424, 0, 0, 1.410429, 353.003082, 204.288284)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 72.713 0.156 Q 70.315 0.156 68.178 -0.782 Q 66.041 -1.72 64.425 -3.414 Q 62.809 -5.108 61.871 -7.402 Q 60.933 -9.695 60.933 -12.406 Q 60.933 -15.064 61.871 -17.384 Q 62.809 -19.703 64.451 -21.476 Q 66.093 -23.248 68.23 -24.238 Q 70.367 -25.229 72.713 -25.229 Q 75.215 -25.229 77.196 -24.238 Q 79.177 -23.248 80.558 -21.528 Q 81.939 -19.808 82.669 -17.488 Q 83.399 -15.169 83.451 -12.51 Q 83.451 -9.852 82.747 -7.558 Q 82.044 -5.265 80.662 -3.518 Q 79.281 -1.772 77.274 -0.808 Q 75.267 0.156 72.713 0.156 Z M 73.547 -5.682 Q 75.319 -5.682 76.779 -6.542 Q 78.238 -7.402 79.098 -8.94 Q 79.959 -10.477 79.959 -12.406 Q 79.959 -14.387 79.098 -15.872 Q 78.238 -17.358 76.805 -18.244 Q 75.371 -19.13 73.547 -19.13 Q 71.723 -19.13 70.263 -18.27 Q 68.804 -17.41 67.944 -15.898 Q 67.083 -14.387 67.083 -12.406 Q 67.083 -10.477 67.944 -8.94 Q 68.804 -7.402 70.289 -6.542 Q 71.775 -5.682 73.547 -5.682 Z M 83.138 10.999 Q 81.679 10.999 81.001 10.581 Q 80.323 10.164 80.167 9.461 Q 80.011 8.757 80.011 7.871 L 80.011 -23.352 Q 80.219 -24.03 80.61 -24.421 Q 81.001 -24.812 81.653 -24.994 Q 82.304 -25.177 83.19 -25.177 Q 84.598 -25.177 85.249 -24.734 Q 85.901 -24.291 86.057 -23.613 Q 86.214 -22.935 86.214 -22.049 L 86.214 7.923 Q 86.214 8.809 86.057 9.487 Q 85.901 10.164 85.249 10.581 Q 84.598 10.999 83.138 10.999 Z" transform="matrix(1.410424, 0, 0, 1.410429, 353.003082, 204.288284)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 100.11 0.469 Q 97.712 0.469 95.705 -0.391 Q 93.698 -1.251 92.186 -2.867 Q 90.675 -4.483 89.841 -6.724 Q 89.007 -8.966 89.007 -11.676 L 89.007 -22.049 Q 89.007 -22.935 89.163 -23.639 Q 89.32 -24.343 89.997 -24.76 Q 90.675 -25.177 92.082 -25.177 Q 93.594 -25.177 94.245 -24.76 Q 94.897 -24.343 95.053 -23.613 Q 95.21 -22.883 95.21 -21.997 L 95.21 -11.676 Q 95.21 -9.8 95.861 -8.444 Q 96.513 -7.089 97.764 -6.333 Q 99.015 -5.577 100.839 -5.577 Q 102.716 -5.577 104.045 -6.333 Q 105.374 -7.089 106.104 -8.47 Q 106.834 -9.852 106.834 -11.676 L 106.834 -22.101 Q 106.834 -22.987 106.99 -23.691 Q 107.147 -24.395 107.824 -24.786 Q 108.502 -25.177 109.961 -25.177 Q 111.421 -25.177 112.046 -24.76 Q 112.672 -24.343 112.854 -23.613 Q 113.037 -22.883 113.037 -22.049 L 113.037 -2.502 Q 113.037 -1.72 112.854 -1.043 Q 112.672 -0.365 112.02 0.052 Q 111.369 0.469 109.909 0.469 Q 108.867 0.469 108.267 0.235 Q 107.668 0 107.381 -0.391 Q 107.094 -0.782 107.016 -1.199 Q 106.938 -1.616 106.938 -2.033 L 107.355 -3.44 Q 107.042 -2.971 106.417 -2.32 Q 105.791 -1.668 104.879 -1.016 Q 103.967 -0.365 102.768 0.052 Q 101.569 0.469 100.11 0.469 Z" transform="matrix(1.410424, 0, 0, 1.410429, 353.003082, 204.288284)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 128.7 0.834 Q 125.312 0.834 122.784 -0.261 Q 120.256 -1.355 118.562 -3.258 Q 116.868 -5.16 116.008 -7.532 Q 115.148 -9.904 115.148 -12.458 Q 115.148 -16.211 116.868 -19.13 Q 118.588 -22.049 121.637 -23.717 Q 124.687 -25.385 128.648 -25.385 Q 131.463 -25.385 133.6 -24.499 Q 135.737 -23.613 137.171 -22.179 Q 138.604 -20.746 139.36 -19.026 Q 140.116 -17.306 140.116 -15.69 Q 140.116 -12.927 138.787 -11.676 Q 137.457 -10.425 135.477 -10.425 L 121.716 -10.425 Q 121.768 -8.653 122.836 -7.35 Q 123.905 -6.047 125.547 -5.343 Q 127.189 -4.639 128.909 -4.639 Q 130.212 -4.639 131.228 -4.796 Q 132.245 -4.952 132.975 -5.186 Q 133.704 -5.421 134.252 -5.682 Q 134.799 -5.942 135.268 -6.151 Q 135.737 -6.359 136.154 -6.411 Q 136.728 -6.464 137.301 -6.151 Q 137.874 -5.838 138.239 -5.056 Q 138.552 -4.535 138.682 -4.092 Q 138.813 -3.649 138.813 -3.232 Q 138.813 -2.189 137.562 -1.277 Q 136.311 -0.365 134.043 0.235 Q 131.776 0.834 128.7 0.834 Z M 121.716 -14.126 L 132.401 -14.126 Q 133.287 -14.126 133.757 -14.413 Q 134.226 -14.699 134.226 -15.638 Q 134.226 -16.837 133.496 -17.801 Q 132.766 -18.765 131.515 -19.313 Q 130.264 -19.86 128.596 -19.86 Q 126.667 -19.86 125.104 -19.104 Q 123.54 -18.348 122.628 -17.045 Q 121.716 -15.742 121.716 -14.126 Z" transform="matrix(1.410424, 0, 0, 1.410429, 353.003082, 204.288284)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 154.119 0.521 Q 150.835 0.521 148.776 -0.469 Q 146.717 -1.46 145.752 -3.492 Q 144.788 -5.525 144.788 -8.601 L 144.788 -18.713 L 142.495 -18.609 Q 141.244 -18.609 140.722 -19.339 Q 140.201 -20.068 140.201 -21.736 Q 140.201 -23.352 140.774 -24.082 Q 141.348 -24.812 142.651 -24.812 L 144.788 -24.708 L 144.788 -30.702 Q 144.788 -31.64 144.971 -32.318 Q 145.153 -32.996 145.805 -33.386 Q 146.456 -33.777 147.863 -33.777 Q 149.323 -33.777 149.975 -33.36 Q 150.626 -32.943 150.809 -32.24 Q 150.991 -31.536 150.991 -30.702 L 150.991 -24.708 Q 151.721 -24.76 152.946 -24.76 Q 154.171 -24.76 155.161 -24.812 Q 156.308 -24.812 157.611 -24.786 Q 158.914 -24.76 159.592 -24.76 L 159.592 -30.702 Q 159.592 -31.64 159.748 -32.318 Q 159.904 -32.996 160.556 -33.386 Q 161.208 -33.777 162.667 -33.777 Q 164.075 -33.777 164.726 -33.36 Q 165.378 -32.943 165.56 -32.24 Q 165.743 -31.536 165.743 -30.702 L 165.743 -24.655 L 169.965 -24.812 Q 170.851 -24.812 171.555 -24.629 Q 172.258 -24.447 172.675 -23.795 Q 173.092 -23.144 173.092 -21.684 Q 173.092 -20.225 172.675 -19.573 Q 172.258 -18.922 171.555 -18.765 Q 170.851 -18.609 170.017 -18.609 L 165.743 -18.713 L 165.743 -8.809 Q 165.743 -7.298 166.055 -6.594 Q 166.368 -5.89 167.124 -5.656 Q 167.88 -5.421 169.131 -5.421 Q 170.121 -5.421 170.825 -5.265 Q 171.529 -5.108 171.919 -4.509 Q 172.31 -3.909 172.31 -2.554 Q 172.31 -1.095 171.867 -0.443 Q 171.424 0.209 170.694 0.365 Q 169.965 0.521 169.131 0.521 Q 165.847 0.521 163.736 -0.469 Q 161.625 -1.46 160.608 -3.492 Q 159.592 -5.525 159.592 -8.601 L 159.592 -18.713 Q 159.383 -18.713 158.627 -18.687 Q 157.872 -18.661 156.933 -18.635 Q 155.995 -18.609 155.317 -18.609 Q 154.379 -18.609 153.102 -18.635 Q 151.825 -18.661 150.991 -18.713 L 150.991 -8.809 Q 150.991 -7.298 151.252 -6.594 Q 151.512 -5.89 152.19 -5.656 Q 152.868 -5.421 154.119 -5.421 Q 155.109 -5.421 155.813 -5.265 Q 156.516 -5.108 156.881 -4.509 Q 157.246 -3.909 157.246 -2.554 Q 157.246 -1.095 156.803 -0.443 Q 156.36 0.209 155.656 0.365 Q 154.953 0.521 154.119 0.521 Z" transform="matrix(1.410424, 0, 0, 1.410429, 353.003082, 204.288284)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 4.185 0.369 Q 3.189 0.369 2.788 0.055 Q 2.387 -0.258 2.401 -0.756 Q 2.414 -1.254 2.57 -1.88 L 5.99 -15.596 Q 6.137 -16.186 6.39 -16.683 Q 6.643 -17.181 7.178 -17.476 Q 7.712 -17.771 8.745 -17.771 Q 9.777 -17.771 10.164 -17.476 Q 10.552 -17.181 10.533 -16.665 Q 10.515 -16.149 10.359 -15.522 L 6.949 -1.843 Q 6.792 -1.217 6.558 -0.719 Q 6.323 -0.221 5.789 0.074 Q 5.254 0.369 4.185 0.369 Z M 9.599 -21.347 Q 8.53 -21.347 8.111 -21.661 Q 7.691 -21.974 7.7 -22.527 Q 7.709 -23.08 7.874 -23.744 Q 8.049 -24.444 8.306 -24.961 Q 8.564 -25.477 9.159 -25.79 Q 9.753 -26.103 10.822 -26.103 Q 11.928 -26.103 12.325 -25.772 Q 12.721 -25.44 12.735 -24.905 Q 12.75 -24.371 12.575 -23.67 Q 12.419 -23.043 12.133 -22.49 Q 11.848 -21.937 11.295 -21.642 Q 10.742 -21.347 9.599 -21.347 Z" transform="matrix(1.410424, 0, 0, 1.410429, 603.657349, 173.728989)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 14.762 0.59 Q 13.804 0.59 12.785 0.387 Q 11.766 0.184 10.867 -0.203 Q 9.968 -0.59 9.383 -1.125 Q 8.797 -1.659 8.778 -2.323 Q 8.723 -2.691 8.847 -3.042 Q 8.971 -3.392 9.252 -3.779 Q 9.533 -4.166 10.003 -4.572 Q 10.436 -4.977 10.8 -5.033 Q 11.164 -5.088 11.643 -4.793 Q 12.077 -4.609 12.574 -4.314 Q 13.072 -4.019 13.607 -3.724 Q 14.142 -3.429 14.741 -3.244 Q 15.34 -3.06 15.967 -3.06 Q 17.331 -3.06 18.257 -3.521 Q 19.183 -3.982 19.395 -4.83 Q 19.514 -5.309 19.348 -5.678 Q 19.182 -6.047 18.818 -6.286 Q 18.454 -6.526 17.933 -6.729 Q 17.412 -6.931 16.808 -7.097 Q 16.204 -7.263 15.559 -7.484 Q 14.701 -7.743 13.872 -8.111 Q 13.042 -8.48 12.47 -9.07 Q 11.898 -9.66 11.676 -10.545 Q 11.454 -11.429 11.785 -12.757 Q 12.199 -14.416 13.281 -15.577 Q 14.363 -16.739 16.031 -17.365 Q 17.699 -17.992 19.838 -17.992 Q 20.575 -17.992 21.23 -17.882 Q 21.884 -17.771 22.479 -17.568 Q 23.073 -17.365 23.608 -17.07 Q 24.143 -16.776 24.604 -16.407 Q 25.397 -15.891 25.264 -15.209 Q 25.131 -14.527 24.385 -13.752 Q 23.85 -13.236 23.394 -13.033 Q 22.938 -12.831 22.542 -13.015 Q 22.025 -13.31 21.431 -13.66 Q 20.836 -14.01 20.181 -14.268 Q 19.527 -14.527 18.752 -14.527 Q 18.015 -14.527 17.37 -14.305 Q 16.725 -14.084 16.329 -13.679 Q 15.933 -13.273 15.795 -12.72 Q 15.666 -12.204 15.878 -11.872 Q 16.091 -11.54 16.515 -11.319 Q 16.939 -11.098 17.561 -10.932 Q 18.183 -10.766 18.884 -10.618 Q 19.861 -10.397 20.848 -10.065 Q 21.834 -9.733 22.59 -9.144 Q 23.347 -8.554 23.656 -7.577 Q 23.966 -6.6 23.579 -5.051 Q 22.908 -2.36 20.568 -0.885 Q 18.228 0.59 14.762 0.59 Z" transform="matrix(1.410424, 0, 0, 1.410429, 603.657349, 173.728989)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
                <path d="M 25.526 0.369 Q 24.494 0.369 24.111 0.055 Q 23.729 -0.258 23.747 -0.774 Q 23.765 -1.29 23.912 -1.88 L 29.4 -23.891 Q 29.556 -24.518 29.809 -25.016 Q 30.062 -25.514 30.597 -25.809 Q 31.131 -26.103 32.164 -26.103 Q 33.196 -26.103 33.579 -25.79 Q 33.961 -25.477 33.966 -24.979 Q 33.971 -24.481 33.815 -23.854 L 31.737 -15.522 Q 32.686 -16.37 34.027 -17.089 Q 35.368 -17.808 37.027 -17.808 Q 39.276 -17.808 40.774 -16.72 Q 42.273 -15.633 42.827 -13.715 Q 43.381 -11.798 42.738 -9.217 L 40.899 -1.843 Q 40.752 -1.254 40.518 -0.756 Q 40.283 -0.258 39.749 0.037 Q 39.214 0.332 38.182 0.332 Q 37.149 0.332 36.744 0.037 Q 36.338 -0.258 36.351 -0.756 Q 36.365 -1.254 36.53 -1.917 L 38.36 -9.254 Q 38.681 -10.545 38.478 -11.503 Q 38.275 -12.462 37.541 -12.996 Q 36.808 -13.531 35.518 -13.531 Q 33.785 -13.531 32.352 -12.517 Q 30.92 -11.503 30.34 -9.918 L 28.327 -1.843 Q 28.18 -1.254 27.922 -0.737 Q 27.664 -0.221 27.13 0.074 Q 26.596 0.369 25.526 0.369 Z" transform="matrix(1.410424, 0, 0, 1.410429, 603.657349, 173.728989)" style="fill: rgb(51, 51, 51); line-height: 83.4009px; white-space: pre;"/>
              </g>
            </g>
          </svg>
        </a>
        <input type="checkbox" id="toggler">
        <label for="toggler">
          <i class="hamburguer">
            <span class="bar"></span>
            <span class="bar"></span>
            <span class="bar"></span>
          </i>
        </label>
        <div class="menu">
          <ul class="list">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Contacto</a></li>
            <li><a href="#">Nosotros</a></li>
          </ul>
        </div>
      </nav>
    `;
  }
};
customElements.define('header-navbar', headerNavBar);
//##################################################################################################################
class BasicHero extends LitElement {
  static get is() {
    return 'basic-hero';
  }

  createRenderRoot() {
    return this;
  }

  static get styles() {
    return [];
  }

  static get properties() {
    return {
      side: { type: String },
      header: { type: String },
      text: { type: String },
      visible: { type: Boolean, attribute: false },
      _cta: { type: String, attribute: false }
    };
  }

  constructor() {
    super();
    this.visible = false;
  }

  connectedCallback() {
    super.connectedCallback();
    this.observeVisibility();
  }

  observeVisibility() {
    const observer = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting) {
        this.visible = true;
        observer.disconnect();
      }
    });

    observer.observe(this);
  }

  render() {
    const numImg = Math.floor(Math.random() * 4) + 1;
    return html`
      <div class="basic-hero-container">
        <div class="hero-bg-container">
          <div class="hero-bg${this.visible ? ' visible' + numImg : ''}"></div>
        </div>
        <div class="hero-text ${this.side}">
          <h1 class="animated-title">
            <span class="text-top">
              <span>
                <span>${seo.h1}</span>
              </span>
            </span>
          </h1>
          <h2 class="animated-subtitle">
            <span class="text-bottom">
              <span>${seo.h2}</span>
            </span>
          </h2>
          <a href="https://wa.me/623298625?text=¡Hola!%20Me%20gustaría%20hacer%20una%20consulta" target="_blank" class="whatsapp-btn" rel="noopener noreferrer nofollow" title="¡Consúltame por whatsapp!" alt="¡Consúltame por whatsapp!">
            <svg class="whatsapp" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 175.216 175.552">
              <defs>
                <linearGradient id="b" x1="85.915" x2="86.535" y1="32.567" y2="137.092" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#57d163"/>
                  <stop offset="1" stop-color="#23b33a"/>
                </linearGradient>
                <filter id="a" width="1.115" height="1.114" x="-.057" y="-.057" color-interpolation-filters="sRGB">
                  <feGaussianBlur stdDeviation="3.531"/>
                </filter>
              </defs>
              <path fill="#b3b3b3" d="m54.532 138.45 2.235 1.324c9.387 5.571 20.15 8.518 31.126 8.523h.023c33.707 0 61.139-27.426 61.153-61.135.006-16.335-6.349-31.696-17.895-43.251A60.75 60.75 0 0 0 87.94 25.983c-33.733 0-61.166 27.423-61.178 61.13a60.98 60.98 0 0 0 9.349 32.535l1.455 2.312-6.179 22.558zm-40.811 23.544L24.16 123.88c-6.438-11.154-9.825-23.808-9.821-36.772.017-40.556 33.021-73.55 73.578-73.55 19.681.01 38.154 7.669 52.047 21.572s21.537 32.383 21.53 52.037c-.018 40.553-33.027 73.553-73.578 73.553h-.032c-12.313-.005-24.412-3.094-35.159-8.954zm0 0" filter="url(#a)"/><path fill="#fff" d="m12.966 161.238 10.439-38.114a73.42 73.42 0 0 1-9.821-36.772c.017-40.556 33.021-73.55 73.578-73.55 19.681.01 38.154 7.669 52.047 21.572s21.537 32.383 21.53 52.037c-.018 40.553-33.027 73.553-73.578 73.553h-.032c-12.313-.005-24.412-3.094-35.159-8.954z"/><path fill="url(#linearGradient1780)" d="M87.184 25.227c-33.733 0-61.166 27.423-61.178 61.13a60.98 60.98 0 0 0 9.349 32.535l1.455 2.312-6.179 22.559 23.146-6.069 2.235 1.324c9.387 5.571 20.15 8.518 31.126 8.524h.023c33.707 0 61.14-27.426 61.153-61.135a60.75 60.75 0 0 0-17.895-43.251 60.75 60.75 0 0 0-43.235-17.929z"/><path fill="url(#b)" d="M87.184 25.227c-33.733 0-61.166 27.423-61.178 61.13a60.98 60.98 0 0 0 9.349 32.535l1.455 2.313-6.179 22.558 23.146-6.069 2.235 1.324c9.387 5.571 20.15 8.517 31.126 8.523h.023c33.707 0 61.14-27.426 61.153-61.135a60.75 60.75 0 0 0-17.895-43.251 60.75 60.75 0 0 0-43.235-17.928z"/><path fill="#fff" fill-rule="evenodd" d="M68.772 55.603c-1.378-3.061-2.828-3.123-4.137-3.176l-3.524-.043c-1.226 0-3.218.46-4.902 2.3s-6.435 6.287-6.435 15.332 6.588 17.785 7.506 19.013 12.718 20.381 31.405 27.75c15.529 6.124 18.689 4.906 22.061 4.6s10.877-4.447 12.408-8.74 1.532-7.971 1.073-8.74-1.685-1.226-3.525-2.146-10.877-5.367-12.562-5.981-2.91-.919-4.137.921-4.746 5.979-5.819 7.206-2.144 1.381-3.984.462-7.76-2.861-14.784-9.124c-5.465-4.873-9.154-10.891-10.228-12.73s-.114-2.835.808-3.751c.825-.824 1.838-2.147 2.759-3.22s1.224-1.84 1.836-3.065.307-2.301-.153-3.22-4.032-10.011-5.666-13.647"/>
            </svg>
            <div class="cta-text">¡Consúltame!</div>
          </a>
          <div class="cta-catalog">
            <a class="cta-catalog-link" href="#catalogo">Ver productos</a>
          </div>
        </div>
      </div>
    `;
  }
}
customElements.define('basic-hero', BasicHero);
//##################################################################################################################
class textComponent extends LitElement {

  static get is() {
    return 'text-component';
  }

  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {
      titulo: { type: String },
      texto: { type: String }
    };
  }

  constructor() {
    super();
    this.titulo = textos.titulo;
    this.textos = textos.textos;
  }

  async connectedCallback() {
    super.connectedCallback();
  }

  writeparagraphs() {
    let paragraphs = [];
    for (let i = 0; i < this.textos.length; i++) {
      paragraphs[i] = html`<p>${this.textos[i]}</p>`;
    }
    return paragraphs;
  }

  render() {
    return html`
        <div class="contenedor">
          <div class="content">
          <h2 class="titulo">${this.titulo}</h2>
          <div class="textos">
          ${this.writeparagraphs()}
          </div>
          </div>
        </div>
      `;
  };
}
customElements.define('text-component', textComponent);
//##################################################################################################################
/**
* Publish / Subscribe Pattern
* To comunicate between components
*/
class PubSub {
  constructor() {
    this.events = [];
  }

  subscribe(eventName, fn) {
    if (!this.events[eventName]) {
      this.events[eventName] = [];
    }
    this.events[eventName].push(fn);
  }

  unsubscribe(eventName, fn) {
    if (!this.events[eventName]) return;
    const index = this.events[eventName].indexOf(fn);
    if (index > -1) {
      this.events[eventName].splice(index, 1);
    }
  }

  publish(eventName, data) {
    if (!this.events[eventName]) return;
    this.events[eventName].forEach(fn => {
      fn(data);
    });
  }
}
const pubsub = new PubSub();
//##################################################################################################################
class ShoppingCart extends LitElement {
  static get is() {
    return 'shopping-cart';
  }

  createRenderRoot() {
    return this;
  }

  static get styles() {
    return [];
  }

  static get properties() {
    return {
      open: { type: Boolean },
      products: { type: Array },
      productsList: { type: Array },
      options: { type: Array },
      total: { type: Number }
    };
  }

  constructor() {
    super();
    this.open = false;
    this.products = [];
    this.productsList = [];
    this.options = Array.from({ length: 100 }, (_, i) => i + 1);
    this.total = 0;
    this.handleCustomMessage = this.handleCustomMessage.bind(this);
  }

  connectedCallback() {
    super.connectedCallback();
    pubsub.subscribe('customMessage', this.handleCustomMessage);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    pubsub.unsubscribe('customMessage', this.handleCustomMessage);
  }

  handleCustomMessage(price) {
    let code = price.msg;
    this.open = true;
    let newItem = {
      "price": code,
      "quantity": 1,
      "adjustable_quantity": {
        "enabled": true,
        "minimum": 0,
        "maximum": this.options.length
      }
    };
    if (!this.products.some(objeto => objeto.price === newItem.price)) {
      const posDB = products.list.findIndex(objeto => objeto.code === code);
      if (products.list[posDB].enStock) {
        this.products.push(newItem);
        this.productsList.push({ ...products.list[posDB] });
        this.total += products.list[posDB].price;
      }
    } else {
      const pos = this.products.findIndex(objeto => objeto.price === code);
      if (this.products[pos].quantity < this.options.length) {
        this.products[pos].quantity++;
        this.productsList[pos].quantity++;
        let subtotal = this.productsList[pos].price * this.productsList[pos].quantity;
        this.productsList[pos].subtotal = subtotal.toFixed(2);
        this.total += this.productsList[pos].price;
      }
    }
    this.requestUpdate();
  }

  _openPanel(ev) {
    this.open = !this.open;
  }

  /**
   * Invocación de una función Lambda desde un navegador
   * Debe estar cargada previamente la librería de AWS-SDK, pero únicamente con los servicios usados: cognito y lambdas
   * Para personalizar la librería usamos https://sdk.amazonaws.com/builder/js/ y alojamos el fichero resultante en nuestro hosting.
   * 
   * https://docs.aws.amazon.com/es_es/sdk-for-javascript/v3/developer-guide/cross_LambdaForBrowser_javascript_topic.html
   */
  _tramitarPedido() {
    // Configure AWS SDK for JavaScript & set region and credentials
    // Initialize the Amazon Cognito credentials provider
    // Lazy Load AWS SDK Script
    let awsScript = window.document.createElement('script');
    awsScript.setAttribute('src', './js/aws-sdk-2.1350.0.min.js');
    window.document.head.appendChild(awsScript);
    let productos = this.products;
    awsScript.addEventListener('load', function () {
      AWS.config.region = 'eu-west-1'; // Region
      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'eu-west-1:e373b2f5-8299-4b9a-bc61-7cdbb5ac8830',
      });
      let lambda = new AWS.Lambda({ region: 'eu-west-1', apiVersion: '2015-03-31' });
      const params = {
        FunctionName: 'checkout-stripe', /* required */
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify({ "body": { "web": products.shop, "products": productos } }), /* (Buffer, Typed Array, Blob, String) */
        LogType: 'None'
      };
      console.log(params);
      // Call the Lambda function https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html#invoke-property
      lambda.invoke(params, function (err, data) {
        if (err) {
          prompt(err);
        } else {
          let response = JSON.parse(data.Payload);
          // console.log(response);
          if (response.headers.Location) {
            if (response.headers && response.headers.Location !== '') {
              window.location.href = response.headers.Location;
            }
          } else {
            console.log(response.errorMessage);
          }
        }
      });
    });
  }

  _onOptionSelected(ev) {
    let idProducto = parseInt(ev.target.dataset.idproduct);
    this.total -= this.productsList[idProducto].price * this.productsList[idProducto].quantity;
    this.products[idProducto].quantity = parseInt(ev.target.value);
    this.productsList[idProducto].quantity = parseInt(ev.target.value);
    let subtotal = this.productsList[idProducto].price * this.productsList[idProducto].quantity;
    this.productsList[idProducto].subtotal = subtotal.toFixed(2);
    this.total += subtotal;
    this.requestUpdate();
  }

  _delProduct(ev) {
    let idProducto = parseInt(ev.currentTarget.dataset.idprod);
    this.total -= this.productsList[idProducto].subtotal;
    this.products.splice(idProducto, 1);
    this.productsList.splice(idProducto, 1);
    this.requestUpdate();
  }

  render() {
    return html`
      <div class="icon" @click="${this._openPanel}" title="Abrir carrito" alt="Abrir carrito">${this._shoppingCartIcon()}</div>
      <div id="right-sidenav" class="sidenav${this.open ? ' open' : ''}">
        <div class="cabecera-carrito">
          <div class="closebtn" @click="${this._openPanel}" title="Cerrar carrito" alt="Cerrar carrito">\&times;</div>
        </div>
        <div class="header-products">Lista de productos</div>
        <div class="products-cart">
          ${this.productsList.map((product, idproduct) => {
      return html`
              <div class="cart-product">
                <div class="prodImg">
                  <img class="product-img" src="./img/${product.img}_66x66.webp" alt="${product.title}" title="${product.title}" loading="lazy" />
                </div>
                <div class="prodName">
                  <div class="prodDel" data-idprod="${idproduct}" @click="${this._delProduct}">${this._trashCartIcon()}</div>
                  <div class="product-title">${product.title}</div>
                  ${product.description}
                </div>
                <div class="prodNumbers">
                  <div class="quantity">
                    <select @change="${this._onOptionSelected}" data-idproduct="${idproduct}">
                      ${this.options.map(option => html`
                        <option value="${option}" ?selected="${option === product.quantity}">
                          ${option}
                        </option>
                      `)}
                    </select>
                  </div>
                  <div> x </div>
                  <div class="price">${product.price}€</div>
                  <div> = </div>
                  <div class="subtotal">${product.subtotal}€</div>
                </div>
                <span class="header-products">IVA Incluído.</span>
              </div>
            `;
    })}
          ${this.productsList.length === 0 ? html`<div class="cart-products-empty">Lista vacía</div>` : html``}
          <div class="gastos">
            <div class="gasto">
              <div class="title">Gastos de envío:</div>
              <div class="header-products description">España (Península Ibérica) 2,81€</div>
            </div>
          </div>
          <div class="total">
            <div class="title">Total: ${this.total > 0 ? (this.total + 2.81).toFixed(2) : 0} €</div>
          </div>
        </div>
        <a href="https://wa.me/623298625?text=¡Hola!%20Me%20gustaría%20hacer%20una%20consulta" target="_blank" class="whatsapp-btn-shop" rel="noopener noreferrer nofollow" title="¡Consúltame por whatsapp!" alt="¡Consúltame por whatsapp!">
          <svg class="whatsapp" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 175.216 175.552">
            <defs>
              <linearGradient id="b" x1="85.915" x2="86.535" y1="32.567" y2="137.092" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#57d163"/>
                <stop offset="1" stop-color="#23b33a"/>
              </linearGradient>
              <filter id="a" width="1.115" height="1.114" x="-.057" y="-.057" color-interpolation-filters="sRGB">
                <feGaussianBlur stdDeviation="3.531"/>
              </filter>
            </defs>
            <path fill="#b3b3b3" d="m54.532 138.45 2.235 1.324c9.387 5.571 20.15 8.518 31.126 8.523h.023c33.707 0 61.139-27.426 61.153-61.135.006-16.335-6.349-31.696-17.895-43.251A60.75 60.75 0 0 0 87.94 25.983c-33.733 0-61.166 27.423-61.178 61.13a60.98 60.98 0 0 0 9.349 32.535l1.455 2.312-6.179 22.558zm-40.811 23.544L24.16 123.88c-6.438-11.154-9.825-23.808-9.821-36.772.017-40.556 33.021-73.55 73.578-73.55 19.681.01 38.154 7.669 52.047 21.572s21.537 32.383 21.53 52.037c-.018 40.553-33.027 73.553-73.578 73.553h-.032c-12.313-.005-24.412-3.094-35.159-8.954zm0 0" filter="url(#a)"/><path fill="#fff" d="m12.966 161.238 10.439-38.114a73.42 73.42 0 0 1-9.821-36.772c.017-40.556 33.021-73.55 73.578-73.55 19.681.01 38.154 7.669 52.047 21.572s21.537 32.383 21.53 52.037c-.018 40.553-33.027 73.553-73.578 73.553h-.032c-12.313-.005-24.412-3.094-35.159-8.954z"/><path fill="url(#linearGradient1780)" d="M87.184 25.227c-33.733 0-61.166 27.423-61.178 61.13a60.98 60.98 0 0 0 9.349 32.535l1.455 2.312-6.179 22.559 23.146-6.069 2.235 1.324c9.387 5.571 20.15 8.518 31.126 8.524h.023c33.707 0 61.14-27.426 61.153-61.135a60.75 60.75 0 0 0-17.895-43.251 60.75 60.75 0 0 0-43.235-17.929z"/><path fill="url(#b)" d="M87.184 25.227c-33.733 0-61.166 27.423-61.178 61.13a60.98 60.98 0 0 0 9.349 32.535l1.455 2.313-6.179 22.558 23.146-6.069 2.235 1.324c9.387 5.571 20.15 8.517 31.126 8.523h.023c33.707 0 61.14-27.426 61.153-61.135a60.75 60.75 0 0 0-17.895-43.251 60.75 60.75 0 0 0-43.235-17.928z"/><path fill="#fff" fill-rule="evenodd" d="M68.772 55.603c-1.378-3.061-2.828-3.123-4.137-3.176l-3.524-.043c-1.226 0-3.218.46-4.902 2.3s-6.435 6.287-6.435 15.332 6.588 17.785 7.506 19.013 12.718 20.381 31.405 27.75c15.529 6.124 18.689 4.906 22.061 4.6s10.877-4.447 12.408-8.74 1.532-7.971 1.073-8.74-1.685-1.226-3.525-2.146-10.877-5.367-12.562-5.981-2.91-.919-4.137.921-4.746 5.979-5.819 7.206-2.144 1.381-3.984.462-7.76-2.861-14.784-9.124c-5.465-4.873-9.154-10.891-10.228-12.73s-.114-2.835.808-3.751c.825-.824 1.838-2.147 2.759-3.22s1.224-1.84 1.836-3.065.307-2.301-.153-3.22-4.032-10.011-5.666-13.647"/>
          </svg>
          <div class="cta-text-shop">¡Consúltame!</div>
        </a>
        <div class="action">
          <button class="btn" type="button" @click="${this._tramitarPedido}" name="checkout" ?hidden="${!this.products.length}">Tramitar pedido</button>
        </div>
      </div>
    `;
  }

  _shoppingCartIcon() {
    return html`
      <svg class="carrito" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
        width="24" height="24" viewBox="0 0 256 256" xml:space="preserve">
        <title>Carrito</title>
        <defs></defs>
        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;"
          transform="translate(1.4065934065934016 1.4065934065934016) scale(2.81 2.81)">
          <path
            d="M 73.713 65.44 H 27.689 c -3.566 0 -6.377 -2.578 -6.686 -6.13 c -0.21 -2.426 0.807 -4.605 2.592 -5.939 L 16.381 21.07 c -0.199 -0.889 0.017 -1.819 0.586 -2.53 s 1.431 -1.124 2.341 -1.124 H 87 c 0.972 0 1.884 0.471 2.446 1.263 c 0.563 0.792 0.706 1.808 0.386 2.725 l -7.798 22.344 c -1.091 3.13 -3.798 5.429 -7.063 5.999 l -47.389 8.281 c -0.011 0.001 -0.021 0.003 -0.032 0.005 c -0.228 0.04 -0.623 0.126 -0.568 0.759 c 0.056 0.648 0.48 0.648 0.708 0.648 h 46.024 c 1.657 0 3 1.343 3 3 S 75.37 65.44 73.713 65.44 z"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: white; fill-rule: nonzero; opacity: 1;"
            transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
          <circle cx="28.25" cy="75.8" r="6.5"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: white; fill-rule: nonzero; opacity: 1;"
            transform="  matrix(1 0 0 1 0 0) " />
          <circle cx="68.28999999999999" cy="75.8" r="6.5"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: white; fill-rule: nonzero; opacity: 1;"
            transform="  matrix(1 0 0 1 0 0) " />
          <path
            d="M 19.306 23.417 c -1.374 0 -2.613 -0.95 -2.925 -2.347 l -1.375 -6.155 c -0.554 -2.48 -2.716 -4.212 -5.258 -4.212 H 3 c -1.657 0 -3 -1.343 -3 -3 s 1.343 -3 3 -3 h 6.749 c 5.372 0 9.942 3.662 11.113 8.904 l 1.375 6.155 c 0.361 1.617 -0.657 3.221 -2.274 3.582 C 19.742 23.393 19.522 23.417 19.306 23.417 z"
            style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: white; fill-rule: nonzero; opacity: 1;"
            transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
        </g>
      </svg>
    `;
  }

  _trashCartIcon() {
    return html`
      <svg class="papelera" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 105.7 122.88" width="16" height="16">
        <title>eliminar</title>
        <path d="M30.46,14.57V5.22A5.18,5.18,0,0,1,32,1.55v0A5.19,5.19,0,0,1,35.68,0H70a5.22,5.22,0,0,1,3.67,1.53l0,0a5.22,5.22,0,0,1,1.53,3.67v9.35h27.08a3.36,3.36,0,0,1,3.38,3.37V29.58A3.38,3.38,0,0,1,102.32,33H98.51l-8.3,87.22a3,3,0,0,1-2.95,2.69H18.43a3,3,0,0,1-3-2.95L7.19,33H3.37A3.38,3.38,0,0,1,0,29.58V17.94a3.36,3.36,0,0,1,3.37-3.37Zm36.27,0V8.51H39v6.06ZM49.48,49.25a3.4,3.4,0,0,1,6.8,0v51.81a3.4,3.4,0,1,1-6.8,0V49.25ZM69.59,49a3.4,3.4,0,1,1,6.78.42L73,101.27a3.4,3.4,0,0,1-6.78-.43L69.59,49Zm-40.26.42A3.39,3.39,0,1,1,36.1,49l3.41,51.8a3.39,3.39,0,1,1-6.77.43L29.33,49.46ZM92.51,33.38H13.19l7.94,83.55H84.56l8-83.55Z"/>
      </svg>
    `;
  }
};
customElements.define('shopping-cart', ShoppingCart);
//##################################################################################################################
class ItemsGallery extends LitElement {
  static get is() {
    return 'items-gallery';
  }

  createRenderRoot() {
    return this;
  }

  static get styles() {
    return [];
  }

  static get properties() {
    return {
      products: { type: Array }
    };
  }

  constructor() {
    super();
    this.products = [];
  }

  async connectedCallback() {
    super.connectedCallback();
    this.products = products.list.sort((a, b) => a.title.localeCompare(b.title));
  }

  render() {
    return html`
      <div itemscope itemtype="https://schema.org/ItemList">
        <div id="catalogo" class="gallery">
          ${this.products.map(
      product => html`
              <gallery-item
                class="gallery-item"
                img="${product.img}"
                title="${product.title}"
                description="${product.description}"
                price="${product.price}"
                code="${product.code}"
                enstock="${product.enStock}"
              ></gallery-item>
            `
    )}
        </div>
      </div>
    `;
  }
};
customElements.define('items-gallery', ItemsGallery);
//##################################################################################################################
class GalleryItem extends LitElement {
  static get is() {
    return 'gallery-item';
  }

  createRenderRoot() {
    return this;
  }

  static get styles() {
    return [];
  }

  static get properties() {
    return {
      img: { type: String },
      title: { type: String },
      description: { type: String },
      price: { type: Number },
      code: { type: String },
      enStock: { type: String },
    };
  }

  constructor() {
    super();
  }

  render() {
    let inStock = this.enStock === 'true';
    return html`
      <div class="gallery-item-wrapper" itemprop="itemListElement" itemscope itemtype="https://schema.org/Product">
        <picture class="pic-gallery-item">
          <source srcset="./img/${this.img}_450x450.webp" media="(min-width: 992px)">
          <source srcset="./img/${this.img}_450x450.webp" media="(min-width: 656px and max-width: 991px)">
          <source srcset="./img/${this.img}_450x450.webp" media="(min-width: 451px and max-width: 655px)">
          <source srcset="./img/${this.img}_450x450.webp" media="(min-width: 351px and max-width: 450px)">
          <source srcset="./img/${this.img}_300x300.webp" media="(max-width: 350px)">
          <img
            class="img-gallery-item"
            itemprop="image"
            src="./img/${this.img}_450x450.webp"
            type="image/webp"
            title="${this.title}"
            alt="${this.title}"
            loading="lazy"
          >
        </picture>
        <h3 itemprop="name">${this.title}</h3>
        <div itemprop="offers" itemscope itemtype="https://schema.org/Offer">
          <div class="price${inStock ? '' : ' disabled'}" itemprop="price" content="${this.price.toFixed(2)}">
            ${this.price.toFixed(2)}<span itemprop="priceCurrency" content="EUR">€</span>
            <span class="stock-status${inStock ? '' : ' disabled'}" itemprop="availability" content="https://schema.org/InStock">
              (${inStock ? 'en stock' : 'agotado'})
            </span>
          </div>
        </div>
        <div class="cta">
          <button class="btn${inStock ? '' : ' disabled'}" @click="${this.publishMessage}" type="button" name="add-basket" ?disabled="${inStock ? '' : ' disabled'}">Añadir al carrito</button>
        </div>
        <p itemprop="description">${this.description}</p>
        
      </div>
      `;
  }

  publishMessage() {
    const message = { msg: this.code };
    pubsub.publish('customMessage', message);
  }
};
customElements.define('gallery-item', GalleryItem);
//##################################################################################################################
let stylesBaseTag = css``;
class baseTag extends LitElement {
  static get is() {
    return 'base-tag';
  }

  createRenderRoot() {
    return this;
  }

  static get styles() {
    return [stylesBaseTag];
  }

  static get properties() {
    return {
      variable: { type: Array }
    };
  }

  constructor() {
    super();
    this.variable = [];
  }

  async connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
      <div class="">
      </div>
    `;
  }
};
customElements.define('base-tag', baseTag);